import Koa from 'koa';
import test from 'ava';
import errorMiddleware from '../../middleware/error';

test('should allow things to happen after it if there are no errors', async (assert) => {
    let didHappen = false;

    await errorMiddleware({app: new Koa()}, async () => {
        didHappen = true;
    });

    if (didHappen) {
        assert.pass();
    } else {
        assert.fail();
    }
});

test('should catch and print error messages', async (assert) => {
    const content = {app: new Koa()};
    const errorMessage = 'Error message';

    content.app.on('error', () => {
        // ... do nothing
    });

    await errorMiddleware(content, async () => {
        throw new Error(errorMessage);
    });

    assert.is(content.body.error.message, errorMessage);
});

test('should print error codes', async (assert) => {
    const content = {app: new Koa()};
    const errorCode = 'error';

    content.app.on('error', () => {
        // ... do nothing
    });

    await errorMiddleware(content, async () => {
        const error = new Error();
        error.code = errorCode;
        throw error;
    });

    assert.is(content.body.error.code, errorCode);
});

test('should set context status', async (assert) => {
    const content = {app: new Koa()};
    const errorStatus = 401;

    content.app.on('error', () => {
        // ... do nothing
    });

    await errorMiddleware(content, async () => {
        const error = new Error();
        error.status = errorStatus;
        throw error;
    });

    assert.is(content.status, errorStatus);
});

test('should do nothing if there is no error', async (assert) => {
    const content = {app: new Koa()};
    const body = 'hello';

    await errorMiddleware(content, async () => {
        content.body = body;
    });

    assert.deepEqual(content.body, body);
});

test('should emit errors to an app', async (assert) => {
    assert.plan(2);

    const content = {
        app: {
            emit: (event, error) => {
                assert.is(event, 'error');
                assert.is(error.code, 'sillyerror');
            }
        }
    };

    await errorMiddleware(content, async () => {
        const error = new Error('silly error');
        error.code = 'sillyerror';

        throw error;
    });
});

test('should not emit errors to an app if status is 404', async (assert) => {
    assert.plan(1);

    const content = {
        app: {
            emit: () => {
                assert.fail('Error was emit');
            }
        }
    };

    await errorMiddleware(content, async () => {
        const error = new Error('silly error');
        error.code = 'sillyerror';
        error.status = 404;

        throw error;
    });

    assert.pass();
});
