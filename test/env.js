import test from 'ava';
import * as env from '../env';

test('environment should be environment', (assert) => {
    assert.is(env.environment, process.env.NODE_ENV);
});
