import escapeStringRegExp from 'escape-string-regexp';
import isString from '@amphibian/is-string';
import isRegExp from '@amphibian/is-regexp';
import inArray from '@amphibian/in-array';
import objectHasProperty from '@amphibian/object-has-property';
import iterateUpArray from '@amphibian/iterate-up-array';
import map from '@amphibian/map';
import errors from '@amphibian/errors';
import {validate, expectOneToSucceed} from '@amphibian/validate';

const routeMiddlewareSchema = {
    type: 'object',
    properties: {
        app: {type: 'object'},
        paths: {type: 'object'},
        handler: {type: 'function'},
        options: {
            type: 'object',
            properties: {
                method: {type: 'string', optional: true},
                path: {
                    type: 'any',
                    test: (input, objectPath) => {
                        try {
                            expectOneToSucceed([
                                () => validate(input, {type: 'string'}),
                                () => validate(input, {type: 'regexp'})
                            ]);
                        } catch (error) {
                            const oldData = error.data.slice();

                            error.data[0] = objectPath;
                            error.data[1] = 'string|regexp';
                            error.message = error.message.replace(
                                oldData.join(' '),
                                error.data.join(' ')
                            );

                            throw error;
                        }
                    }
                }
            }
        }
    }
};

/**
 * Router middleware
 * @param {object} app
 * @param {object} paths
 * @param {function} handler
 * @param {object} options
**/
function routeMiddleware(app, paths, handler, options) {
    validate({app, paths, handler, options}, routeMiddlewareSchema);

    const {method, path} = options;
    const uppercasedMethod = (method)
        ? method.toUpperCase()
        : 'GET';

    if (!objectHasProperty(paths, path)) {
        paths[path] = [];
    }

    if (!inArray(uppercasedMethod, paths[path])) {
        paths[path].push(uppercasedMethod);
    }

    let doesPathMatch;
    let getPathArguments = () => null;

    if (isString(path)) {
        const parameterRegExp = /(:[^/]+?)(?![^/])/g;
        const parameters = map(path.match(parameterRegExp), (parameter) => (
            parameter.slice(1)
        ));

        if (!parameters || (parameters.length === 0)) {
            doesPathMatch = (targetPath) => path === targetPath;
        } else {
            const pathRegExp = new RegExp(`^${escapeStringRegExp(path).replace(parameterRegExp, '([^/]+)')}$`);

            doesPathMatch = (targetPath) => pathRegExp.test(targetPath);
            getPathArguments = (targetPath) => {
                const matches = targetPath.match(pathRegExp);
                const args = {};

                matches.shift();

                iterateUpArray(matches, (value, i) => {
                    args[parameters[i]] = value;
                });

                return args;
            };
        }
    } else if (isRegExp(path)) {
        doesPathMatch = (targetPath) => path.test(targetPath);
        getPathArguments = (targetPath) => {
            const args = targetPath.match(path);
            args.shift();
            return args;
        };
    }

    const routeHandler = async (context, next) => {
        const requestPath = context.path;

        if (doesPathMatch(requestPath)) {
            const allowedMethods = paths[path];
            context.routePath = path;

            if (context.method === 'OPTIONS') {
                context.status = 200;
                context.body = allowedMethods.join(', ');
                context.set('cache-control', 's-maxage=180, maxage=180');

                return;
            } else if (context.method === uppercasedMethod) {
                context.args = getPathArguments(requestPath);
                await handler(context, next);

                return;
            }

            if (!inArray(context.method, allowedMethods)) {
                throw errors.methodNotAllowed();
            }
        }

        await next(context, next);
    };

    if (handler.name) {
        Object.defineProperty(routeHandler, 'name', {value: handler.name});
    }

    return routeHandler;
}

const createRouterSchema = {
    type: 'object',
    properties: {
        app: {type: 'object'}
    }
};

/**
 * Create router
 * @param {object} app
 *
 * @returns {function} routeMiddleware
**/
function createRouter(app) {
    validate({app}, createRouterSchema);
    const paths = {};
    return routeMiddleware.bind(null, app, paths);
}

export default createRouter;
export {routeMiddleware};
