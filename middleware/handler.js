import chalk from 'chalk';
import isFunction from '@amphibian/is-function';
import {validate} from '@amphibian/validate';

const registerHandlerSchema = {
    type: 'object',
    properties: {
        app: {type: 'object'},
        handlers: {type: 'array'},
        type: {type: 'string'},
        name: {type: 'string'},
        handler: {type: 'function'}
    }
};

/**
 * Register handler to use in the app
 * @param {object} app
 * @param {array} handlers
 * @param {string} type
 * @param {string} name
 * @param {function} handler
**/
function registerHandler(app, handlers, type, name, handler) {
    if (isFunction(name) && !handler) {
        handler = name;
        ({name} = handler);
    }

    if (!name) {
        name = (handler) ? (handler.name || 'anonymous') : 'anonymous';
    }

    validate({app, handlers, type, name, handler}, registerHandlerSchema);
    handlers.push(handler);
    app.emit(`handlers(${type}):did-add`, name || handler.name);
}

/**
 * Get the next handler
 * @param {array} handlers
 * @param {number} i
 * @param {object} context
 * @param {function} next
 *
 * @returns {function} next
**/
function nextHandler(handlers, i, context, next) {
    const handler = handlers[i];

    if (handler) {
        return async () => {
            await handler(context,
                await nextHandler(handlers, i + 1, context, next)
            );
        };
    }

    return async () => {
        await next(context);
    };
}

/**
 * Handler middleware that sets up registered handlers
 * @param {array} handlers
 * @param {object} context
 * @param {function} next
**/
async function handlerMiddleware(handlers, context, next) {
    if (handlers.length > 0) {
        await handlers[0](context, await nextHandler(handlers, 1, context, next));
    } else {
        await next(context);
    }
}

const createHandlerSchema = {
    type: 'object',
    properties: {
        app: {type: 'object'}
    }
};

/**
 * Create handler
 * @param {object} app
 *
 * @returns {function} handlerMiddleware
**/
function createHandler(app) {
    validate({app}, createHandlerSchema);
    const handlers = [];

    app.on('handlers(handler):did-add', (name) => {
        app.emit('log',
            chalk.green('[handler]'),
            'Registered handler',
            chalk.blue(name)
        );
    });

    app.on('handlers(middleware):did-add', (name) => {
        app.emit('log',
            chalk.green('[handler]'),
            'Registered middleware',
            chalk.cyan(name)
        );
    });

    return {
        handlers,
        handlerMiddleware: handlerMiddleware.bind(null, handlers),
        registerMiddleware: registerHandler.bind(null, app, handlers, 'middleware'),
        registerHandler: registerHandler.bind(null, app, handlers, 'handler')
    };
}

export default createHandler;
export {
    registerHandler,
    handlerMiddleware
};
