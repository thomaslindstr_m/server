import errors from '@amphibian/errors';

const notFoundError = errors.notFound();

/**
 * Not Found (404) middleware that throws a 404 not found error
**/
export default function notFoundMiddleware() {
    throw notFoundError;
}
